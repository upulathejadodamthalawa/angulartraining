﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace MyMovie
{
    /// <summary>
    /// Summary description for MovieWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MovieWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello Upul";
        }

        [WebMethod(Description = "Returns all the movies")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Movie> GetMovies()
        {
            DataSet data = new DataSet();
            SqlConnection conn = new SqlConnection();
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["movieConnection"].ConnectionString);
            conn.Open();
            SqlCommand cmd =
                new SqlCommand(
                    "SELECT Id,Name,Description,ReleaseYear,DirectedBy,Language,Rating,Characters FROM Movie ORDER BY ReleaseYear DESC",
                    conn);
            // cmd.CommandType = CommandType.Text;  
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                adapter.SelectCommand = cmd;
            }
            catch
            {
                return null;
            }
            adapter.Fill(data, "Movies");

            List<Movie> list = new List<Movie>();
            foreach (DataRow row in data.Tables[0].Rows)
                list.Add(new Movie
                {
                    Id = row["Id"].ToString(),
                    Name = row["Name"].ToString(),
                    Description = row["Description"].ToString(),
                    DirectedBy = row["DirectedBy"].ToString(),
                    Rating = row["Rating"].ToString(),
                    Language = row["Language"].ToString(),
                    Characters = row["Characters"].ToString(),
                    ReleaseYear = row["ReleaseYear"].ToString()
                });

            //return list;
            return list;
        }

        [WebMethod(Description = "Returns all the movies")]
        public string SaveMovie(Movie Movie)
        {
            return "success";
        }

    }
}
