﻿angular.module('list').directive('movieListItem', movieListItem);

function movieListItem() {
    return {
        restrict: 'AE',
        scope: {
            movie: '='
        },
        template: `
            //<div movie-directive rating-value="movie.Rating" max="5" readonly="true"></div>
            <h3>{{movie.Name}}</h3>
            <p>{{movie.Description}}</p>
            <a ui-sref="detail({ id: movie.id })">Detail<a/>
            `
    };
}