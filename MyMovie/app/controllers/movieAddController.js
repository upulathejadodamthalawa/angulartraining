﻿movieApp.controller("MovieAddController", ['$scope', function ($scope, movieData) {

        $scope.submit = function() {
            var movie = [{ "name": $scope.name, "description": $scope.description, "director": $scope.director, "releaseyear": $scope.releaseyear, "rating": $scope.rating, "language": $scope.language }];

            movieData.saveMovie(movie).success(function(succ) {
                $scope.msg = succ;
            }).error(function(error) {
                alert(error);
            });
        }
    }
]);