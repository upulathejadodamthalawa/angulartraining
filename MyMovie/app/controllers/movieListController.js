﻿movieApp.controller('MovieListController', function ($scope, movieData) {
    $scope.title = 'My Movies';
    getMovies();

    function getMovies() {
        movieData.getMovies().success(function(movies) {
            $scope.movieList = movies;
        }).error(function(error) {
            alert(error);
        });
    }

    $scope.ViewDetails = function (Id) {
        $location.path("movieDetails.html/"+Id);
    }

    $scope.AddMovie = function () {
        window.location ="addMovie.html";
    }
});