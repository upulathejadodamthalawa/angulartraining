﻿movieApp.factory("movieData", ['$http', function ($http) {

        var urlBase = 'MovieWebService.asmx';
        var movieData = {};

        movieData.getMovies = function() {
            return $http.get(urlBase + '/GetMovies');
        };

        movieData.saveMovie = function(movie) {
            return $http.post(urlBase + '/SaveMovie', movie);
        };
        return movieData;

    }
]);