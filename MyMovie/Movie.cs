﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Web;

namespace MyMovie
{
    public class Movie
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReleaseYear { get; set; }
        public string DirectedBy { get; set; }
        public string Language { get; set; }
        public string Rating { get; set; }
        public string Characters { get; set; }
    }
}